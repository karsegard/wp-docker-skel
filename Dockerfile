FROM kda/infomaniak-8.0

#ENV APACHE_DOCUMENT_ROOT /app/web
#RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
#RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN chmod ugo+w /var/www
RUN mkdir -p /var/www/html/wp-content/themes
WORKDIR /var/www/html/
RUN chown -R www-data:www-data *

RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
RUN mv wp-cli.phar /usr/local/bin/wp
RUN chmod ugo+x /usr/local/bin/wp

#COPY import /var/www/
#COPY scripts /var/www/


USER www-data

#CMD ["/var/www/scripts/setup-wp.sh"]
#RUN bash /var/www/setup-wp.sh
