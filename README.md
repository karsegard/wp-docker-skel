# link your theme 

ln -s yourtheme theme

docker-compose -f docker-compose-dev.yml  up -d
docker-compose -f docker-compose-dev.yml  down
docker-compose -f docker-compose-dev.yml  build


# setup wp 

docker exec -it wp-docker-skel-wordpress-1 bash /var/www/scripts/setup-wp.sh