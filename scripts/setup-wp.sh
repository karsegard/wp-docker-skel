#!/bin/bash
DIRNAME=$(dirname $0)

if test -f "/var/www/.setup"; then
    echo "already setup skip"
    exit;
fi

cd /var/www/html
wp core download
wp core config --dbhost=$WP_DB_HOST --dbname=$WP_DB --dbuser=$WP_DB_USER --dbpass=$WP_DB_PASSWORD
chmod 600 wp-config.php
wp core install --url=$WP_DOMAIN --title="$WP_TITLE" --admin_name=$WP_ADMIN_NAME --admin_password=$WP_ADMIN_PASSWORD --admin_email=$WP_ADMIN_EMAIL


wp language core install fr_FR
wp language core update
wp site switch-language fr_FR

wp plugin update --all
wp plugin uninstall  hello


#wp plugin install wordpress-importer --activate
#wp import ../import.xml --authors=create



wp theme delete twentytwentyone
wp theme delete twentytwentytwo

wp comment delete 1
wp post delete 1
wp post delete 2
wp post delete 3

source $DIRNAME/import.sh




source $DIRNAME/plugin.sh


wp search-replace $WP_LIVE_URL http://$WP_DOMAIN --all-tables

ln -s /var/www/theme /var/www/html/wp-content/themes/dev_theme
wp theme list
